import React, { useState , useEffect } from "react";
import { Router } from 'react-router-dom';
import history from './services/history';
import Routes from './routes';
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"

import { AuthContext } from "./context/auth";
import {loadFromLocalStorage,saveToLocalStorage} from './components/localStorage'

function App() {

    const [authTokens, setAuthTokens] = useState(loadFromLocalStorage() !== undefined || false);

    const setTokens = (data) => {
        if(data){
            saveToLocalStorage(data);
            setAuthTokens(true);
        }
    }

    // useEffect(() => {
    //     let localStorage = loadFromLocalStorage()
    //     console.log(localStorage)
    //     setTokens(localStorage)
    //     // localStorage.setItem('myValueInLocalStorage', value);
    //     console.log(authTokens)
    // });
    console.log(authTokens);
    return (
        <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
            <Router history={history}>
                <Routes auth={authTokens}/>
            </Router>
        </AuthContext.Provider>
    );
}

export default App;