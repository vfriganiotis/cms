import React, {useState} from 'react';
import axios from 'axios';
import {DashboardContext} from "../../context/dashboard";
import styled from "styled-components";
import {  FormTextarea, FormInput, Button, Modal, ModalBody, ModalHeader } from "shards-react";

const Span = styled.span`
  cursor: pointer;
  display: block;
  float: left;
  width: 100%;
`;

const black = '#000';

const Div = styled.div`
    float: left;
    border-bottom: 1px solid ${black};
    margin-bottom: 20px;
`;

const Textarea= styled.textarea`
    width: 100%;
    display: block;
`;

const ReviewsContainer = () => {

    const [modalReview, setModalReview] = useState(false);
    const [modalState, setModalState] = useState();
    const _setModalState = (review) => {
        if(review){
            setModalState(review);
        }
        setModalReview(!modalReview);
    }

    return (
        <DashboardContext.Consumer>
            {value => (
                <div>
                    <div>{value && hotels(value)}</div>
                    <div>{value && reviews(value,_setModalState)}</div>
                    {modalReview && _updateTheReview(modalState,_setModalState,value)}
                </div>
            )}
        </DashboardContext.Consumer>
    )
}

const reviews = (items,modalState) => {
    if(items.dashboard){
        return(
            <ul>
                {items.dashboard.reviews && items.dashboard.reviews.map( item => reviewDiv(item,items.setDashboard,modalState) )}
            </ul>
        )
    }
}

const _updateReviews = (id,context) => {
    axios.get('/reviews/fiilterByHotel?hotel=' + id)
        .then(function (response) {
            console.log(response.data)
            context(response.data);
        })
        .catch(function (error) {
            console.log(error);
        })
        .finally(function () {
            // always executed
        });
}

const _updateSingleReview = (id,context,form,modal) => {
    console.log(id)
    console.log(context)
    console.log(form)
    axios.post('/reviews/updateReviewById',form)
        .then(function (response) {

            console.log(response.data)
            let objIndex = context.dashboard.reviews.findIndex((obj => obj._id == id));
            context.dashboard.reviews[objIndex].name = response.data.name
            context.dashboard.reviews[objIndex].heading = response.data.heading
            context.dashboard.reviews[objIndex].content = response.data.content
            context.setDashboard(context.dashboard);
            modal()
            // context(response.data.reviews[]);

        })
        .catch(function (error) {
            console.log(error);
        })
        .finally(function () {
            // always executed
        });
}

const _updateTheReview  = (item,_setModalState,context) => {
    return(
        <Modal centered={true} open={true} toggle={_setModalState}>
            <ModalHeader>{item.name}</ModalHeader>
            <ModalBody>
                {/*<FormInput size="sm" placeholder={item._id} className="mb-2" />*/}
                {/*<Span>NAME: <FormInput size="sm" placeholder={item.name} className="mb-2" /></Span>*/}
                {/*<Span>HEADING: <FormInput size="sm" placeholder={item.heading} className="mb-2" /></Span>*/}
                {/*<Span>CONTENT: <textarea name='awesome' value={item.content} /></Span>*/}
                {/*<Span>LINK: <FormInput size="sm" placeholder={item.link} className="mb-2" /></Span>*/}
                {/*<Span>DATE: <FormInput size="sm" placeholder={item.date} className="mb-2" /></Span>*/}
                {/*<Span>CITY: <FormInput size="sm" placeholder={item.country} className="mb-2" /></Span>*/}
                {/*<Span>COUNTRY: <FormInput size="sm" placeholder={item.heading} className="mb-2" /></Span>*/}
                <Editor clearModal={_setModalState} _id={item._id} name={item.name} heading={item.heading} content={item.content} update={context}  />
            </ModalBody>
        </Modal>
        )
}

const Editor = (props) => {
    const [name, setName] = useState(props.name);
    const [heading, setHeading] = useState(props.heading);
    const [content, setContent] = useState(props.content);

    const handleName = (event) => {
        setName(event.target.value);
    };

    const handleHeading = (event) => {
        setHeading(event.target.value);
    };

    const handleContent = (event) => {
        setContent(event.target.value);
    };

    const updateReview = () => {
        let form = {
            name : name,
            heading: heading,
            content: content,
            _id: props._id
        }

        _updateSingleReview(props._id,props.update,form,props.clearModal)
    }

    return (
        <div>
            <FormInput size="sm" placeholder={props._id} className="mb-2" />
            <Span>NAME: <FormInput size="sm" placeholder={name} className="mb-2" onChange={handleName}/></Span>
            <Span>HEADING: <FormInput size="sm" placeholder={heading} className="mb-2" onChange={handleHeading}/></Span>
            <Textarea name="textarea" value={content} onChange={handleContent} />
            <Button onClick={() => updateReview(props)}>UPDATE</Button>
        </div>
    );
}

const reviewDiv = (item,contect,modalState) => <Div onClick={() => modalState(item)}>
    <Span>NAME: {item.name}</Span>
    <Span>HEADING: {item.heading}</Span>
    <Span>CONTENT: {item.content}</Span>
    <Span>LINK: {item.link}</Span>
    <Span>DATE: {item.date}</Span>
    <Span>CITY: {item.city}</Span>
    <Span>COUNTRY: {item.country}</Span>
    <Span>HOTEL_ID: {item.hotel_id}</Span>
</Div>

const hotels = items => {
    if(items.dashboard){
        return(
            <ul>
                {items.dashboard.hotels && items.dashboard.hotels.map( item => hotelDiv(item,items.setDashboard))}
            </ul>
        )
    }
}

const hotelDiv = (item,context) => <Div onClick={() => _updateReviews(item._id,context)}>
    <Span>NAME: {item.name}</Span>
    <Span>ID: {item._id}</Span>
    <Span>USERNAME: {item.username}</Span>
</Div>

export default ReviewsContainer