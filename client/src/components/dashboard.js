import React, { useState, useEffect } from 'react';
import axios from 'axios';
import styled from 'styled-components';
import Account from './account'
import ReviewsContainer from './dashboard/reviews'

import { loadFromDashboardStorage,saveToDashboardStorage } from '../localstorage/dashboardStorage'
import { DashboardContext,useDashboard } from "../context/dashboard";

const Dashboard = () => {

    const [dashboard, setDashboard] = useState(loadFromDashboardStorage() !== undefined ? loadFromDashboardStorage() : null);

    const _setDashboard = (data) => {
        if(data){
            saveToDashboardStorage(data);
            setDashboard(data);
        }
    }

    useEffect(function(){
        console.log(dashboard)
        if(!dashboard){
            axios.get('/reviews/dashboard')
                .then(function (response) {
                    console.log(response);
                    setDashboard(response.data);
                    saveToDashboardStorage(response.data)
                })
                .catch(function (error) {
                    console.log(error);
                })
                .finally(function () {
                    // always executed
                });
        }
    },[]);

    return (
        <DashboardContext.Provider value={{ dashboard, setDashboard: _setDashboard }}>
            <div>
                <Account/>
                <ReviewsContainer/>
            </div>
        </DashboardContext.Provider>
    );
};

export default Dashboard
