import React from 'react';
import axios from 'axios';
import { AuthContext } from "../context/auth";

class RegisterForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: 'coconut'};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({value: event.target.value});
    }
    handleSubmit(context){

        console.log(context)
        // e.preventDefault()
        const email = document.getElementById('email').value;
        const password = document.getElementById('password').value;
        const name = document.getElementById('name').value;

        const {authTokens,setAuthTokens} = context
        console.log(authTokens)
        console.log(setAuthTokens)
        // vv()
        console.log('aww')
        axios({
            method: "POST",
            url: "http://localhost:3005/users/users",
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            },
            data: {
                email: email,
                name: name,
                password: password
            },
            crossDomain: true
        }).then((response)=>{

            console.log(response)
            if (response.status === 201){
                alert('succes')
                setAuthTokens(response.data.token)
                window.location.href = 'http://localhost:3005/dashboard';
            }else{
                alert('fail')
            }

        }).catch(error => {
            alert('Catch on errr')
        })

    }

    render() {
        return (
            <AuthContext.Consumer>
                {value => (
                    <form>
                        <h1>Register</h1>
                        <label htmlFor="name"><b>Name</b></label>
                        <input type="text" id={"name"} placeholder="Enter Name" name="name" required />
                        <label htmlFor="email"><b>Email</b></label>
                        <input type="text" id={"email"} placeholder="Enter Email" name="email" required />
                        <label htmlFor="psw"><b>Password</b></label>
                        <input type="password" id={"password"} placeholder="Enter Password" name="psw" required />
                        <input onClick={() => this.handleSubmit(value)} value="Submit"/>
                    </form>
                )}
            </AuthContext.Consumer>
        );
    }
}

export default RegisterForm;