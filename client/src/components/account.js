import React from 'react';
import axios from 'axios';
import {AuthContext} from "../context/auth";

class Account extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: 'coconut'};
        this.getMyAccount = this.getMyAccount.bind(this);
        this.logOut = this.logOut.bind(this);
        this.logOutAll = this.logOutAll.bind(this);
    }

    getMyAccount(e){
        e.preventDefault();
        let token = JSON.parse(localStorage.getItem('state'));
        axios({
            method: "GET",
            url: "http://localhost:3005/users/users/me",
            headers: {
                'Authorization': 'Bearer ' + token
            },
            crossDomain: true
        }).then((response)=>{
            console.log(response)
            // window.location.href = 'http://localhost:3005/users/users/me';
        }).catch(error => {
            alert('Catch on errr')
        })
    }

    logOut(context){

        const {authTokens,setAuthTokens} = context
        console.log(authTokens)
        console.log(setAuthTokens)

        let token = JSON.parse(localStorage.getItem('state'));
        console.log(token)
        axios({
            method: "POST",
            url: "http://localhost:3005/users/users/me/logout",
            headers: {
                'Authorization': 'Bearer ' + token
            },
            crossDomain: true
        }).then((response)=>{
            console.log(response)
            localStorage.removeItem('state')
            window.location.href = 'http://localhost:3005/login';
        }).catch(error => {
            alert('Catch on errr')
        })
    }

    logOutAll(e){
        e.preventDefault();
        let token = JSON.parse(localStorage.getItem('state'));
        axios({
            method: "POST",
            url: "http://localhost:3005/users/users/me/logoutall",
            headers: {
                'Authorization': 'Bearer ' + token
            },
            crossDomain: true
        }).then((response)=>{
            console.log(response)
            // window.location.href = 'http://localhost:3005/users/users/me';
        }).catch(error => {
            alert('Catch on errr')
        })
    }

    render() {
        return (
            <AuthContext.Consumer>
                {value => (
                    <div>
                        <div className={"myAccount"} onClick={() => this.getMyAccount(value)}>MY ACCOUNT</div>
                        <div className={"myAccount"} onClick={() => this.logOut(value)}>Log Out</div>
                        <div className={"myAccount"} onClick={() => this.logOutAll(value)}>Log Out From All Devices</div>
                    </div>
                )}
            </AuthContext.Consumer>
        )
    }
}

export default Account;
