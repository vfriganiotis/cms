import React from 'react';
import axios from 'axios';
import { AuthContext } from "../context/auth";

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: 'coconut'};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }
    handleSubmit(context){
        // e.preventDefault();
        const {authTokens,setAuthTokens} = context
        console.log(authTokens)
        console.log(setAuthTokens)

        // const color = React.useContext(AuthContext);
        // console.log('eee')
        // console.log(color)

        const email = document.getElementById('email').value;
        const password = document.getElementById('password').value;

        axios({
            method: "POST",
            url: "http://localhost:3005/users/users/login",
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            },
            data: {
                email: email,
                password: password
            },
            crossDomain: true
        }).then((response)=>{

            setAuthTokens(response.data.token)
            window.location.href = 'http://localhost:3005/dashboard';

        }).catch(error => {
            alert('Catch on errr')
        })

    }

    render() {
        return (
            <AuthContext.Consumer>
                {value => (
                    <form>
                        <h1>LOGIN</h1>
                        <label htmlFor="email"><b>Email</b></label>
                        <input type="text" id={"email"} placeholder="Enter Email" name="email" required />
                        <label htmlFor="password"><b>Password</b></label>
                        <input type="password" id={"password"} placeholder="Enter Password" name="password" required />
                        <input onClick={() => this.handleSubmit(value)} value="Submit"/>
                    </form>
                )}
            </AuthContext.Consumer>
        );
    }
}

export default LoginForm;