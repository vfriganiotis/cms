export const saveToDashboardStorage = state => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('dashboard', serializedState);
    } catch (e) {
        console.log(e);
    }
};
export const loadFromDashboardStorage = () => {
    try {
        const serializedState = localStorage.getItem('dashboard');
        if (serializedState === null) return undefined;
        return JSON.parse(serializedState);
    } catch (e) {
        console.log(e);
        return undefined;
    }
};

