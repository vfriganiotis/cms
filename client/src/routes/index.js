import React from "react";
import { Switch } from "react-router-dom";
import Route from "./Route";

import SignIn from '../components/loginForm';
import RegisterForm from '../components/register';
import Dashboard from '../components/dashboard';

export default function Routes(isAuth) {
    return (
        <Switch>
            <Route path="/" exact component={Dashboard} isPrivate isSigned={isAuth.auth} />
            <Route path="/login" component={SignIn} isSigned={isAuth.auth} />
            <Route path="/register" component={RegisterForm} isSigned={isAuth.auth} />
            <Route path="/dashboard" component={Dashboard} isPrivate isSigned={isAuth.auth}/>
            {/* redirect user to SignIn page if route does not exist and user is not authenticated */}
            <Route component={RootNoFound} />
        </Switch>
    );
}

function RootNoFound(){
    return(
        <div>
            Page Not Found
        </div>
    )
}
