const jwt = require('jsonwebtoken')
const path = require('path')
const User = require(path.join(__dirname , '..', '/models/users/item'))

const auth = async(req, res, next) => {
    if( req.header('Authorization') === undefined ){
       return  res.status(401).send({ error: 'Not authorized to access this resource' })
    }
    const token = req.header('Authorization').replace('Bearer ', '')
    const data = jwt.verify(token,'privateKey')
    try {
        const user = await User.findOne({ _id: data._id, 'tokens.token': token })
        if (!user) {
            throw new Error()
        }
        req.user = user
        req.token = token
        next()
    } catch (error) {
        res.status(401).send({ error: 'Not authorized to access this resource' })
    }
}
module.exports = auth