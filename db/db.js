const mongoose = require('mongoose')
const CONNECTION_URL = 'mongodb+srv://bill:123@cluster0-fs2l4.mongodb.net/users?retryWrites=true&w=majority';
const CONNECTION_URL_REVIEWS = 'mongodb+srv://bill:123@cluster0-fs2l4.mongodb.net/reviews?retryWrites=true&w=majority';

mongoose.set('useFindAndModify', false);

let userMongoose = mongoose.createConnection(CONNECTION_URL,{
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})


let reviewsMongoose = mongoose.createConnection(CONNECTION_URL_REVIEWS,{
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})

module.exports = {
    users : userMongoose,
    reviews : reviewsMongoose
}