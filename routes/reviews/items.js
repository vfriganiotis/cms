const Express = require("express")
const app = Express.Router();
const path = require('path')
const itemModel = require(path.join(__dirname , '..', '..', '/models/reviews/item'))
const hotelModel = require(path.join(__dirname , '..', '..', '/models/reviews/hotel'))
const ObjectID = require('mongodb').ObjectID;

app.get('/',function(req, res){
    res.send('HELLO WORLD');
})

app.get('/updateReviewById',async function(req, res){
    console.log('eeeeeeeeee')
    console.log(req.query.id)

    await itemModel.findOne({_id: req.query.id}, function(err, reviews) {

        console.log(reviews)
        reviews.name = 'testa'
        reviews.save();
        res.json(reviews);
    });



})

app.post('/updateReviewById',async function(req, res){
    console.log('eeeeeeeeee POST')
    console.log(req.body)
    await itemModel.findOne({_id: req.body._id}, function(err, reviews) {
        console.log(reviews)
        reviews.name = req.body.name;
        reviews.heading = req.body.heading;
        reviews.content = req.body.content;
        reviews.save();
        res.json(reviews);
    });

})


app.get('/fiilterByHotel',async function(req, res){
    console.log('wawawwwa')
    console.log(req.query.hotel)
    let data = {
        reviews: null,
        hotels: null
    }

    await hotelModel.find({}, function(err, hotels) {
        data.hotels = hotels
    });

    await itemModel.find({hotel_id: req.query.hotel}, function(err, reviews) {
        data.reviews = reviews
        // console.log(data)
        res.json(data);
    });
})

app.get("/dashboard",async function(req, res) {
    console.log('wawawwwa')
    let data = {
        reviews: null,
        hotels: null
    }

    await hotelModel.find({}, function(err, hotels) {
        data.hotels = hotels
    });

    await itemModel.find({}, function(err, reviews) {
        data.reviews = reviews
        console.log(data)
        res.json(data);
    });

});

app.get("/reviews_api",async function(req, res) {
    let data = []
    await itemModel.find({}, function(err, users) {
        res.json(users);
    });
});

app.get("/add_review",async function(req, res) {

    let msg = new itemModel({
        id: 21,
        remote_id: 1256,
        hotel_id: 225,
        source_id: 1,
        heading: 'Review Heading',
        content : 'Review Content',
        link: '/tripadvisor.html',
        date: '20 June 2019',
        city: 'Athens',
        country: 'Greece',
        score: 5
    })

    msg.save()
        .then(doc => {
            console.log(doc)
        })
        .catch(err => {
            console.error(err)
        })

})

// app.get("/hotels",async function(req, res) {
//     let data = []
//     await itemModel.find({}, function(err, users) {
//         res.json(users);
//     });
// });

app.get("/add_hotel",async function(req, res) {

    let msg = new hotelModel({
        name: 'hotel1',
        host : 'hostHotel1',
        username: 'hotelUsername',
        password: 'hotelPassword',
        database: 'databaseHotel1'
    })

    msg.save()
        .then(doc => {
            console.log(doc)
        })
        .catch(err => {
            console.error(err)
        })

})

app.get("/add_source",async function(req, res) {

    let msg = new hotelModel({
        name: 'hotel1',
        host : 'hostHotel1',
        username: 'hotelUsername',
        password: 'hotelPassword',
        database: 'databaseHotel1',
        id: 'hotelid1'
    })

    msg.save()
        .then(doc => {
            console.log(doc)
        })
        .catch(err => {
            console.error(err)
        })

})

module.exports = app