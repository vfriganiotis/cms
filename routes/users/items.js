const Express = require("express")
const app = Express.Router();
const path = require('path')
const User = require(path.join(__dirname , '..', '..', '/models/users/item'))
const auth = require('../../middleware/auth')

app.get('/',function(req, res){

    res.send('HELLO WORLD Users');
})

app.get("/users_api",async function(req, res) {

    let data = []
    let counter = 10;
    await User.find({}, function(err, users) {
        res.json(users);
    });

});

app.get("/add_user",async function(req, res) {

    let msg = new User({
        username: 'First UserName 11111111111111',
        password: 'First Password',
        name: 'dddd',
        lastName: 'Myname is',
        city: 'Athens',
        country: 'Greece'
    })

    msg.save()
        .then(doc => {
            console.log(doc)
        })
        .catch(err => {
            console.error(err)
        })

})

app.post('/users', async (req, res) => {
    // Create a new user
    try {
        const user = new User(req.body)
        await user.save()
        const token = await user.generateAuthToken()
        res.status(201).send({ user, token })
    } catch (error) {
        res.status(400).send(error)
    }
})

app.post('/users/login', async(req, res) => {
    //Login a registered user
    try {
        const { email, password } = req.body

        const user = await User.findByCredentials(email, password)
        if (!user) {
            return res.status(401).send({error: 'Login failed! Check authentication credentials'})
        }
        const token = await user.generateAuthToken()
        res.send({ user, token })
    } catch (error) {
        res.status(400).send(error)
    }

})

app.get('/users/me', auth, async(req, res) => {
    // View logged in user profile
    // res.send(req.user)
    res.send(req.user)
    // res.send(req.user)
})

app.post('/users/me/logout', auth, async (req, res) => {
    // Log user out of the application
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token != req.token
        })
        await req.user.save()
        res.send()
    } catch (error) {
        res.status(500).send(error)
    }
})

app.post('/users/me/logoutall', auth, async(req, res) => {
    // Log user out of all devices
    try {
        req.user.tokens.splice(0, req.user.tokens.length)
        await req.user.save()
        res.send()
    } catch (error) {
        res.status(500).send(error)
    }
})

module.exports = app