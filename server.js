const express = require("express")
const app = express();
const path = require('path')
const port = 3005;
const BodyParser = require("body-parser");

//Get Routes
const reviewsNeW = require('./routes/reviews/items')
const usersNeW = require('./routes/users/items')
//Simple request time logger
app.use(function(req, res, next){
    next();
});

//MidleWare
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));

// app.use('/api/', reviews)

app.use('/reviews/', reviewsNeW)

app.use('/users/', usersNeW)

app.use(express.static('client/build'))

app.get("/create",async function(req, res) {

    res.send('we create');
});

app.get("*",async function(req, res) {
    res.sendFile(path.join(__dirname + '/client/build/index.html'));
});

app.listen(port, function() {
    console.log("Server run on Port " + port)
});