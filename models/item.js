let mongoose = require('mongoose')
let reviewsSchema = new mongoose.Schema({
    heading: String,
    content : String,
    mails: Array
})
module.exports = mongoose.model('Reviews', reviewsSchema)