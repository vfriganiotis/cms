let mongoose = require('mongoose')
let sourceSchema = new mongoose.Schema({
    name: String,
    host : String,
    username: String,
    password: String,
    database: String
})
module.exports = mongoose.model('Sources', sourceSchema)