const mongoose = require('mongoose')
const ObjectId = require('mongodb').ObjectID;

const CONNECTION_URL = 'mongodb+srv://bill:123@cluster0-fs2l4.mongodb.net/reviews?retryWrites=true&w=majority';

const mon = mongoose.createConnection(CONNECTION_URL)

let reviewsSchema = new mongoose.Schema({
    id: Number,
    name: String,
    heading: String,
    content : String,
    date: String,
    city: String,
    country: String,
    remote_id: Number,
    hotel_id: {type:ObjectId, ref: 'hotels'},
    source_id: Number,
    link: String,
    rate: Number
})

module.exports = mon.model('Reviews', reviewsSchema)
