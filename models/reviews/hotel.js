const mongoose = require('mongoose')
const ObjectId = require('mongodb').ObjectID;

const CONNECTION_URL = 'mongodb+srv://bill:123@cluster0-fs2l4.mongodb.net/reviews?retryWrites=true&w=majority';

const mon = mongoose.createConnection(CONNECTION_URL)

let hotelSchema = new mongoose.Schema({
    name: String,
    host : String,
    username: String,
    password: String,
    database: String,
    id: String,
})
module.exports = mon.model('Hotels', hotelSchema)