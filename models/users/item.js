const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const ObjectId = require('mongodb').ObjectID;
const MONGOOSE = require('../../db/db.js')

let userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
        minLength: 7
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }]
})

userSchema.methods.generateAuthToken = async function() {
    // Generate an auth token for the user
    const user = this
    const token = jwt.sign({_id: user._id}, 'privateKey')
    user.tokens = user.tokens.concat({token})
    await user.save()
    return token
}


userSchema.statics.findByCredentials = async (email, password) => {
    // Search for a user by email and password.
    const user = await MONGOOSE.users.model('Users', userSchema).findOne({ email} )
    if (!user) {
        throw new Error({ error: 'Invalid login credentials' })
    }
    // const isPasswordMatch = await bcrypt.compare(password, user.password)
    if (password !== user.password) {
        throw new Error({ error: 'Invalid login credentials' })
    }
    return user
}

module.exports = MONGOOSE.users.model('Users', userSchema)
